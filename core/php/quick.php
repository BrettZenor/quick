<?php

$json_path = "contents.json";
$json_string = quick_file_reference($json_path);
$json = json_decode($json_string, true);

function quick_check_path ($path) {
	global $json;
	$pages = $json['PAGES'];
	$length = count($pages);
	$index = null;
	for ($i = 0; $i < $length; $i++) {
		$url = str_replace(" ", "", strtolower($pages[$i]['TITLE']));
		switch ($path) {
			case null:
				$index = 0;
				break;
			case '':
				$index = 0;
				break;
			case $url:
				$index = $i;
				break;
		}
	}
	return $index;
}

$query = $_GET? $_GET['q']: '';
$index = quick_check_path($query);
$attrs = $json['ATTRS'];
$site = $attrs['SITE'];
$types = $attrs['CONTENT-TYPES'];
$pages = $json['PAGES'];
$page = $pages[$index];
$content_type = $page['CONTENT-TYPE'];

function quick_content () {
	global $index, $types, $page, $content_type;
	
	$output = "";
	$type = $types[$content_type];
	$fields = $type['FIELDS'];
	
	for ($i = 0; $i < count($fields); $i++) {
	
		$field = $fields[$i];
		$label = $field['LABEL'];
		$field_label = $label['TEXT'];
		$value = $page['VALUES'][$i];
		
		if ($label['ENABLED']===true) {
			if ($label['INLINE']===true) {
				$label = "<span class='field-label field-label-".$i."'>".$field_label.": </span>";
			} else {
				$label = "<div class='field-label field-label-".$i."'>".$field_label.": </div>";
			}
		} else {
			$label = "";
		}
		$field_label = strtolower($field_label);
		
		switch ($field['TYPE']) {
			case 'HTML':
				settype($value,"string");
				$output .= "<div class='field-html field-html-".$i." field-html-".$field_label."'>".$label.$value."</div>";
				break;
			case 'TEXT':
				settype($value,"string");
				$output .= "<div class='field-text field-text-".$i." field-text-".$field_label."'>".$label.htmlentities($value)."</div>";
				break;
			case 'INTEGER':
				settype($value,"integer");
				$output .= "<div class='field-integer field-integer-".$i." field-integer-".$field_label."'>".$label.$value."</div>";
				break;
			case 'IMAGE':
				settype($value,"string");
				$output .= "<div class='field-image field-image-".$i." field-image-".$field_label."'>".$label."<img src=".$value." /></div>";
				break;
		}
	}
	
	$title = '<h2 class="page-title page-title-'.$index.'">'.$page['TITLE'].'</h2>';
	return '<div class="page page-'.$index.'">'.$title.$output.'</div>';
}
/**
 * Reference file open and read
 */
function quick_file_reference ($path) {
	$result = "";
	$file = fopen($path,"r") or exit("Unable to open file!");
	$result = fread($file,filesize($path));
	fclose($file);
	return $result;
}
/**
 * Validate if the file exists in array otherwise check next file
 */
function quick_validate ($options) {
	$result = "";
	$len = count($options);
	for ($i = 0; $i < $len; $i++) {
		if (file_exists($options[$i])){
			$result = $options[$i];
			break;
		} else {
			$result = false;
		}
	}
	return $result;
}
/**
 * Remove the extension 
 */
function quick_remove_ext ($path, $type) {
	$result = "";
	$length = strlen($path) - strlen($type);
	$path = str_split($path);
	for ($i = 0; $i < $length; $i++) {
		$result .= $path[$i];
	}
	return $result;
}
/**
 * If field has filetype on it, remove it via quick_remove_filetype
 */
function quick_if_has_ext ($path, $type) {
	$result = explode($type, strtolower($path));
	if (count($result) > 1) {
		$result = quick_remove_ext($path, $type);
	}
	else {
		$result = $result[0];
	}
	return $result;
}
/**
 * build multi-item html strings with content
 */
function quick_loop($case) {
	global $pages, $page, $index;
	
	if ($index === null && $case <> 'menu') {
		$case = 'unknown';
	}
	
	$result = "";
	for($i = 0; $i < count($pages); $i++) {
		$title = $pages[$i]['TITLE'];
		switch ($case) {
			case 'menu':
				$url = str_replace(" ", "", strtolower($title));
				$link = '<a title="'.$title.'" href="'.$url.'">'.$title.'</a>';
				if ($i===$index) {
					$result .= '<li class="menu-item menu-item-'.$i.' active">'.$link.'</li>';
				} else {
					$result .= '<li class="menu-item menu-item-'.$i.'">'.$link.'</li>';
				}
				break;
			case 'mobile':
				$result .= '<option value="'.$i.'">'.$title.'</option>';
				break;
		}
	}
	switch ($case) {
		case 'unknown':
			$page_title = '<h2 class="page-title page-title-unknown">Sorry!</h2>';
			$result = '<div class="page page-unknown">'.$page_title.'This page doesn\'t exist.</div>';
			break;
	}
	return $result;
}
/**
 * render specific items for easy templating base on $case
 */
function quick_render($scope, $case){
	global $attrs, $pages, $logo;
	$result = "";
	
	switch ($case) {
		case 'logo':
			$result = "<a id='logo' href='./'><img src='".$logo."' /></a>";
			break;
		case 'title':
			$result = "<h1><a href='./'>".$attrs['SITE']['TITLE']."</a></h1>";
			break;
		case 'slogan':
			$result = "<div class='slogan-text'>".$attrs['HEADER']['SLOGAN']."</div>";
			break;
		case 'menu':
			$result = "<ul>".quick_loop('menu')."</ul>";
			$result .= "<div id='mobilenav'><select>".quick_loop('mobile')."</select></div>";
			break;
		case 'content':
			$result = quick_content();
			break;
		case 'copyright':
			$result = "<div id='copyright'>&copy;&nbsp;".$attrs['FOOTER']['COPYRIGHT']."&nbsp;<a href='./'>".$attrs['SITE']['TITLE']."</a></div>";
			break;
		case 'search':
			$result = "<div class='search-field'><input type='text' /><input type='submit' value='Search' /></div>";
			break;
		//universal cases
		case 'text':
			$result = "<div class='div-text'>".$attrs[$scope]['TEXT']."</div>";
			break;
		case 'powered':
			$result = "<div class='powered-text'>Powered by Quick CMS</div>";
			break;
	}
	return print($result);
}


/**
 * Validate the theme
 */
$site = $attrs['SITE'];
$title = $index===null? 'Unavailable': $json['PAGES'][$index]['TITLE'];

$theme = quick_if_has_ext($site['THEME'], ".css");
$theme = quick_validate(array("theme/".$theme."/".$theme.".css", "theme/default/default.css", "core/default/theme.css"));

$logo = quick_validate(array("theme/".$theme."/".$attrs['HEADER']['LOGO'], "theme/default/logo.png", "core/default/logo.png"));


/**
 * Encoded json for javascript
 */
$encoded = json_encode($json);

?>