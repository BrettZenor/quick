<!--Begin: HTML template-->

<body>
    <div id='pagewrap'>
	
        <div id="header">
			<?php quick_render('HEADER', 'logo'); ?>
			<?php quick_render('HEADER', 'title'); ?>
			<?php quick_render('HEADER', 'slogan'); ?>
		</div>
		
		<div id="navigation">
			<?php quick_render('', 'menu'); ?>
		</div>
		
		<div id="content">
			<?php quick_render('', 'content'); ?>
		</div>
		
		<div id="footer">
			<?php quick_render('FOOTER', 'copyright'); ?>
			<?php quick_render('FOOTER', 'powered'); ?>
			<?php echo $theme ?>
		</div>
		
    </div><!--EOF:pagewrap-->
</body>

<!--EOF: HTML template-->
