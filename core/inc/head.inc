<!--Link to CSS files-->
<link rel="stylesheet" type="text/css" href="core/css/quick.css" />
<link rel="stylesheet" type="text/css" href="core/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $theme; ?>"/>

<!--Link to JS files-->
<script type="text/javascript" src="core/js/jquery.min.js"></script>
<script type="text/javascript" src="core/js/quick.js"></script>
<script type="text/javascript"> $(document).ready(function(){$.fn.quick(<?php echo $encoded; ?>)}); </script>