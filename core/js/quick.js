/**
 * @file
 * quick.js
 * Author: Brett Zenor
 * Site: http://zaxiscreativemedia.com/
 * Date/Time: 7/29/2013 10:39
 * Comments: 
 * 
 */

(function ($) {
	$.fn.quick = function(site){

		var default_data = {
			"ATTRS": {
				"SITE": {
					"TITLE": "Site Unavailable",
					"THEME": {
						"CSS": "default"
					}
				}
			},
			"PAGES": [
				{
					"TITLE": "Page Unavailable",
					"HTML": "Sorry! No page could be found at this time."
				}
			]
		}
		
		var data = site? site: default_data;
		var pages = data['PAGES'];
		var attrs = data['ATTRS'];
		
		//var header = attrs['HEADER'];
		//var footer = attrs['FOOTER']

		var last;
		var prefix = attrs['SITE']['PREFIX'];
		var title = document.title.split(prefix);
		var titleformat = prefix+title[1];
		
		var length = pages.length; //define the array length
		
		function quick_content(index) {
			var page = pages[index],
			types = attrs['CONTENT-TYPES'],
			content_type = page['CONTENT-TYPE'],
			type = types[content_type],
			fields = type['FIELDS'],
			output = '',field,label,field_label;
			for (var i = 0; i < fields.length; i++) {
				field = fields[i]['TYPE'];
				label = fields[i]['LABEL'];
				field_label = label['TEXT'];
				if (label['ENABLED']===true) {
					if (label['INLINE']===true) {
						label = "<span class='field-label field-label-"+i+"'>"+field_label+": </span>";
					} else {
						label = "<div class='field-label field-label-"+i+"'>"+field_label+": </div>";
					}
				} else {
					label = "";
				}
				value = page['VALUES'][i];
				switch (field) {
					case 'HTML':
						output += "<div class='html-text'>"+label+String(value)+"</div>";
						break;
					case 'TEXT':
						$('body').prepend('<div id="removethis"></div>')
						var div = $('div#removethis');
						value = div.text(value).html();
						div.remove();
						output += "<div class='plain-text'>"+label+value+"</div>";
						break;
					case 'INTEGER':
						output += "<div class='field-integer'>"+label+parseInt(value)+"</div>";
						break;
					case 'IMAGE':
						output += "<div class='field-image'>"+label+"<img src="+String(value)+" /></div>";
						break;
				}
			}
			
			page_title = '<h2 class="page-title page-title-'+index+'">'+page['TITLE']+'</h2>';
			return '<div class="page page-'+index+'" style="display:none;">'+page_title+output+'</div>';
		}
		
		/**
		 * Render content function while 'index' is the integer definition for which content
		 */
		function quick_render(index, if_push){
			if (index==undefined) {
				//$('#content .page').stop(true, true).fadeOut(0);
				//$('#content .page-unknown').fadeIn(300);
				title = '<h2 class="page-title page-title-unknown">Sorry!</h2>',
				pagewrapper = '<div class="page page-unknown" style="display:none;">'+title+'This page doesn\'t exist.</div>';
				
				$('#content').html(pagewrapper);
				$('#content .page').fadeIn(300);
				
				$('.menu-item.active').removeClass('active');
				document.title = "Unavailable "+titleformat;
				last = -1;
			}
			/**
			 * if this page was not just rendered then proceed
			 */
			else if(index!=last){
				console.log(index);
				if(if_push==true) {
					var url = pages[index]['TITLE'].replace(' ', '').toLowerCase();
					history.pushState(null, null, url);
				}
				
				$('.menu-item.active').removeClass('active');
				$('.menu-item-'+index).addClass('active');
				
				content = quick_content(index);
				
				$('#content').html(content);
				$('#content .page').fadeIn(300);
				
				$('#mobilenav select').val(index);
				
				document.title = pages[index]['TITLE']+titleformat;
				last = index;
			}
		}
		
		/**
		 * check_path function takes a url string as an argument when
		 * the onpopstate is used and checks if it matches any page names
		 * if true: it returns an integer of the page to be displayed
		 */
		function quick_check_path (href) {
			var index,
			subdirs = href.split('/'),
			path = subdirs[subdirs.length-1],
			pathname = path;//.replace('-', ' ');
			
			for (var i=0; i<length; i++) {
				switch (pathname) {
					case null:
						index = 0;
						break;
					case '':
						index = 0;
						break;
					case pages[i]['TITLE'].toLowerCase().replace(' ',''):
						index = i;
						break;
				}
			}
			return index;
		}
		
		/**
		 * Add the function to the site including the browsing button popstates
		 */
		function quick_add_function() {
		
			/**
			 * site logo the on click function to render home page only
			 */
			/*$('#header h1').on('click', function(e){
				
				//Render first page (0 based count), usually home page
				quick_render(0, true);
			});*/
			
			/**
			 * All nav menu-items the on click function to render pages
			 */
			/*$('.menu-item').on('click', function(e){
				var index = $(this).index();
				
				e.preventDefault();
				quick_render(index, true);
			});*/
			
			/**
			 * Browsing button onpopstate event triggered to parse the link
			 * name with quick_check_path(location.pathname);
			 */
			window.onpopstate = function(event) {
				var index = quick_check_path(location.pathname);
				
				quick_render(index, false);
			};
			
			/**
			 * Mobile navigation select list on change event to take
			 * the option value and render that page
			 */
			$('#mobilenav select').change(function() {
				var value = $(this).find('option:selected').attr('value');
				
				quick_render(value, true);
			});
			
			$('#pagewrap a:not(.ext)').on('click', function(e){
				var index = quick_check_path($(this).attr('href'));
				
				e.preventDefault();
				quick_render(index, true);
			});
		}
		
		/**
		 * quick_add_contents() adds the html strings per page
		 *//*
		function quick_add_contents() {
			var page, name, url, link, mobilelink, menuitem, title, pagewrapper; 
			for(var i=0; i<length; i++){ 
				page = pages[i],
				name = page['TITLE'],
				url = name.replace(' ','-').toLowerCase();
				
				link = '<a title="'+name+'" href="/'+url+'">'+name+'</a>',
				menuitem = '<li class="menu-item menu-item-'+i+'">'+link+'</li>';
				
				mobilelink = '<option value="'+i+'">'+name+'</option>'
				
				title = '<h2 class="page-title page-title-'+i+'">'+page['TITLE']+'</h2>',
				pagewrapper = '<div class="page page-'+i+'" style="display:none;">'+title+page['HTML']+'</div>';
				/**
				 * append html string consisting of a li and a tags for the navigation
				 * link menu items with dynamic variables substituted
				 *
				$('#mobilenav select').append(mobilelink);
				$('#nav ul').append(menuitem);
				$('#content').append(pagewrapper);
			}
		}*/
		
		/**
		 * when the document is ready
		 */
		$(document).ready(function(){
			//quick_add_contents();
			
			quick_add_function();
			
			/**
			 * after all is completed render the first page's contents (zero based)
			 */
			quick_render(quick_check_path(location.pathname), false);
		}); //end document ready function
		
		return this;
	}
}(jQuery));